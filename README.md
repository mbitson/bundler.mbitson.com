# Magento Bundler - Enhanced Packages and Bundled Products for Magento CE
( Work In Progress ) A magento extension that allows for the creation of 'packages' - which are combinations of other products that are added to the cart in a wizard-like UI, one product at a time. The separation allows some of these products to be recurring, bundled, configurable, or otherwise.


## Bundled Product Enhancements
This package includes the following enhancements to the native Magento Bundled Product:
* Add a limit or requirement to the total quantity of all products added to the bundle.
* Group product options into sections that can have titles and quantity limits/requirements.

## Requirements
This package requires Magento CE 1.9.1.